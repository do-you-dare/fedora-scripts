#!/usr/bin/bash

# TODO: fix commented 'if' so that the script is idempotent

set -e

sudo dnf upgrade -y

# We start with default packages
for file in dnf/*.txt; do
  echo Installing packages from "$file"
  sudo dnf install -y $(cat "$file")
done

# Ruby block
# if command -v rvm > /dev/null; then
  gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys \
    409B6B1796C275462A1703113804BB82D39DC0E3 \
    7D2BAF1CF37B13E2069D6956105BD0E739499BDB

  \curl -sSL https://get.rvm.io | bash -s stable --ruby
  source "$HOME/.rvm/scripts/rvm"

  # Install gems
  GEMS="rubocop bundler neovim"
  for g in $GEMS; do
    gem install "$g"
  done
# fi

# Downloads android studio
(
  mkdir ~/.tmp-android
  cd ~/.tmp-android
  wget https://dl.google.com/dl/android/studio/ide-zips/3.5.0.21/android-studio-ide-191.5791312-linux.tar.gz
  cd /opt
  sudo tar xzf ~/.tmp-android/*.tar.gz
  rm -rf ~/.tmp-android

)

# Install oh-my-zsh
# sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Rust block
# if command -v rustc  > /dev/null 2>&1; then
  # Install cargo
  echo Installing rust
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > ~/.rust_install.sh
  bash ~/.rust_install.sh -y
  rm ~/.rust_install.sh

  source "$HOME/.cargo/env"
  # # We'd need to set oh-my-zsh before doing this -.-
  # echo source "$HOME/.cargo/env" >> "$HOME/.zshrc"

  # Install cargo packages
  echo Installing cargo packages
  CARGO_PACKAGES="ripgrep fd-find tectonic"
  for crate in $CARGO_PACKAGES; do
    cargo install "$crate"
  done
# fi

# Neovim block
if [[ ! -d "$HOME/projects/vimrc" ]]; then
  mkdir -p "$HOME/projects"
  git clone http://gitlab.com/dread-uo/vimrc "$HOME/projects/vimrc"
  (cd "$HOME/projects/vimrc" && ./activate.sh)
fi

echo Android studio is on /opt . Go there, and run the 'studio.sh' script to finish android install.
