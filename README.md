# Fedora scripts

Here I'll find some scripts to setup a fedora machine to my taste whenever I
put my hands on a new machine. Should I be using `ansible` for this? Probably
yes. But time is of essence now, and that's it.

## Usage

Simply run

```bash
./run.sh
```

and be happy :)
